const isThemeRoad = localStorage.getItem('isThemeRoad');
const link = document.createElement('link');
link.rel =  'stylesheet';
link.href = (isThemeRoad === 'true') ? 'css/style-road.css' : 'css/style.css';
document.head.appendChild(link);

function changeTheme() {
    let isThemeRoad = localStorage.getItem('isThemeRoad');
    const link = document.createElement('link');
    link.rel = 'stylesheet';
    if (isThemeRoad === 'true') {
        link.href = 'css/style.css';
        localStorage.setItem('isThemeRoad', 'false');
    } else {
        link.href = 'css/style-road.css';
        localStorage.setItem('isThemeRoad', 'true');
    }
    const linkRemove = document.head.getElementsByTagName('link');
    document.head.appendChild(link);
    document.head.removeChild(linkRemove[2]);
}
